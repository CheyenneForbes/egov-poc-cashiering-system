﻿using Microsoft.Owin;
using Owin;
using System.Data.Entity;
using eGov_POC_Cashiering_System.Models;

[assembly: OwinStartupAttribute(typeof(eGov_POC_Cashiering_System.Startup))]
namespace eGov_POC_Cashiering_System
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            Database.SetInitializer<ApplicationDbContext>(new NullDatabaseInitializer<ApplicationDbContext>());
        }
    }
}
