﻿using System.Web;
using System.Web.Mvc;

namespace eGov_POC_Cashiering_System
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
