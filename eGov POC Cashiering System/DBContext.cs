﻿using eGov_POC_Cashiering_System.Models;
using System.Data.Entity;

namespace eGov_POC_Cashiering_System
{
    public class DBContext : DbContext
    {
        public DBContext() : base("DefaultConnection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            //modelBuilder.Entity<Models.Employee>().ToTable("AspNetUsers");
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Models.LoginTracker> LoginTrackers { get; set; }
        public DbSet<Models.Item> Items { get; set; }
        public DbSet<Models.Transaction> Transactions { get; set; }
        public DbSet<Models.Location> Locations { get; set; }
        public DbSet<Models.Employee> Employees { get; set; }
        public DbSet<Models.EmployeeRole> EmployeeRoles { get; set; }
        public DbSet<Models.Role> EmployeeRoleProperties { get; set; }
        public DbSet<Models.Privilege> Privileges { get; set; }
        public DbSet<Models.RolePrivilege> RolePrivileges { get; set; }
        public DbSet<Models.EODReport> EODReports { get; set; }
    }
}