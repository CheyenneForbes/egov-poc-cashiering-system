﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace eGov_POC_Cashiering_System.Models
{
    public class EODReport
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string ID { get; set; }
        public double DateOpened { get; set; }
        public decimal OpenCash { get; set; }
        public double CloseDate { get; set; }
        public string ClosedBy { get; set; }
        public decimal InHand { get; set; }

        public EODReport()
        {
            ID = "";
            DateOpened = 0;
            OpenCash = 0;
            CloseDate = 0;
            ClosedBy = "";
            InHand = 0;
        }
    }

    public class EODReportsViewModel
    {
        private readonly DBContext db = new DBContext();
        public List<Employee> Employees { get; set; }
        public List<EODReport> EODReports { get; set; }
        public List<Transaction> Transactions { get; set; }

        public EODReportsViewModel()
        {
            EODReports = new List<EODReport>();
            Transactions = new List<Transaction>();
            Employees = db.Employees.ToList();
        }

    }
}
