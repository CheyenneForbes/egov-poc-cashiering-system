﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eGov_POC_Cashiering_System.Models
{
    public class Item
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string ID { get; set; }
        public string Name { get; set; }
        public decimal Cost { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }

        public Item()
        {
            ID = "";
            Name = "";
            Cost = 0;
            Price = 0;
            Quantity = 0;
        }
    }

    public class ItemsViewModel
    {
        public List<Item> Items { get; set; }

        public ItemsViewModel()
        {
            Items = new List<Item>();
        }

    }
}
