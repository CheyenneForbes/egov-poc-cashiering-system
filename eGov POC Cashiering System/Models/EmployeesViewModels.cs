﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace eGov_POC_Cashiering_System.Models
{
    [Table("Privileges")]
    public class Privilege
    {
        [Key]
        public string Name { get; set; }
        public string Group { get; set; }
        public Privilege()
        {

        }
    }

    [Table("RolePrivileges")]
    public class RolePrivilege
    {
        [Key, Column(Order = 0)]
        public string RoleID { get; set; }

        [Key, Column(Order = 1)]
        public string Name { get; set; }

        public RolePrivilege()
        {
            RoleID = "";
        }
    }

    [Table("AspNetRoles")]
    public class Role
    {
        [Key]
        [Column("Id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string ID { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        [ForeignKey("RoleID")]
        public virtual List<RolePrivilege> Privileges { get; set; }

        public Role()
        {
            Name = "";
        }
    }

    [Table("AspNetUserRoles")]
    public class EmployeeRole
    {
        [Key, Column("UserId", Order = 0)]
        public string UserID { get; set; }

        [Key, Column("RoleId", Order = 1)]
        public string ID { get; set; }

        public EmployeeRole()
        {
            UserID = "";
        }
    }

    [Table("AspNetUsers")]
    public class Employee
    {
        [Key]
        [Column("Id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string ID { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        [ForeignKey("UserID")]
        public virtual List<EmployeeRole> Roles { get; set; }
        public string Location { get; set; }

        public Employee()
        {
            ID = "";
            UserName = "";
            Name = "";
            PhoneNumber = "";
            Roles = new List<EmployeeRole>();
            Location = "";
        }
    }

    public class EmployeesViewModel
    {
        //private readonly DBContext db = new DBContext();
        public List<Location> Locations { get; set; }
        public List<Employee> Employees { get; set; }
        public List<Role> EmployeeRoleProperties { get; set; }
        public RegisterViewModel RegisterModel { get; set; }
        public EmployeesViewModel()
        {
            RegisterModel = new RegisterViewModel();
            Locations = new List<Location>();
            EmployeeRoleProperties = new List<Role>();
            Employees = new List<Employee>();
        }

    }
}
