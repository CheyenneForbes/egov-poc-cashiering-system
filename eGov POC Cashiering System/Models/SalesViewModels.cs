﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace eGov_POC_Cashiering_System.Models
{
    public class TransactionItem
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string ID { get; set; }
        public string ReceiptNumber { get; set; }
        public string ItemId { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        [NotMapped]
        public decimal Total {
            get=>(this.Price * (decimal)(this.Quantity));
            set=>this.Total = value;
        }

        public TransactionItem()
        {
            ID = "";
            ItemId = "";
            Name = "";
            Quantity = 1;
            Price = 0;
        }
    }

    public class Transaction
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string ReceiptNumber { get; set; }
        public string Type { get; set; }
        public string CustomerID { get; set; }
        public string PayeeID { get; set; }
        public decimal PaymentAmount { get; set; }
        public string PaymentInstrument { get; set; }
        public double PaymentTimestamp { get; set; }
        public double CancelTimestamp { get; set; }
        public string CashierID { get; set; }
        public string CashierLocation { get; set; }
        [ForeignKey("ReceiptNumber")]
        public List<TransactionItem> Items { get; set; }

        public Transaction()
        {
            Items = new List<TransactionItem>();
            PaymentAmount = 0;
            PaymentTimestamp = 0;
            CancelTimestamp = 0;
    }
    }

    public class SalesViewModel
    {
        private readonly DBContext db = new DBContext();
        public string CustomerName { get; set; }
        public string PayeeName { get; set; }
        public Transaction Transaction { get; set; }
        public List<Item> Items { get; set; }
        public SalesViewModel()
        {
           Items = db.Items.ToList();
            Transaction = new Transaction();
        }
    }
}
