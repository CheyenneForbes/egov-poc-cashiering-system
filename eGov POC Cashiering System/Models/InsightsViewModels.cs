﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace eGov_POC_Cashiering_System.Models
{
    public class InsightsViewModel
    {
        public string InsightType { get; set; }
        public List<Item> Items { get; set; }
        public List<Location> Locations { get; set; }
        public string Location { get; set; }
        public double StartDate { get; set; }
        public double EndDate { get; set; }
        public List<Employee> Employees { get; set; }
        public List<LoginTracker> LoginTrackers { get; set; }
        public string Cashier { get; set; }
        public List<Role> EmployeeRoleProperties { get; set; }
        public List<Transaction> Transactions { get; set; }
        public string SortBy { get; set; }

        public InsightsViewModel()
        {
            StartDate = (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
            EndDate = (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
            InsightType = "CashierTransactionReport";
            SortBy = "PaymentTimestamp";
        }
    }
}
