﻿using eGov_POC_Cashiering_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Microsoft.AspNet.Identity;

namespace eGov_POC_Cashiering_System.App_Code
{
    public class Utilities
    {
        public static bool HasPrivilege(IPrincipal User, string Name)
        {
            DBContext db = new DBContext();
            return User.IsInRole("Administrator") || db.EmployeeRoleProperties.ToList().Any(
                x =>
                    x.Privileges.Exists(x2=> x2.Name == Name)
                    &&
                    User.IsInRole(x.Name)
            );
        }

        public static bool HasPrivilegeGroup(IPrincipal User, string Name)
        {
            DBContext db = new DBContext();
            return User.IsInRole("Administrator") || db.EmployeeRoleProperties.ToList().Any(
                x =>
                    x.Privileges.Exists(x2 => db.Privileges.Find(x2.Name).Group == Name)
                    &&
                    User.IsInRole(x.Name)
            );
        }

        public static bool HigherThanUserRole(IPrincipal User, EmployeeRole Role)
        {
            DBContext db = new DBContext();
            List<Role> EmployeeRoleProperties = db.EmployeeRoleProperties.ToList();
            int Level = EmployeeRoleProperties.Find(x=> x.ID == Role.ID).Level;
            return db.Employees.Find(User.Identity.GetUserId()).Roles.Any(
                x2 => EmployeeRoleProperties.Find(x3 => x3.ID == x2.ID).Level < Level
            );
        }

        public static bool HigherThanUserRole(IPrincipal User, Role Role)
        {
            DBContext db = new DBContext();
            List<Role> EmployeeRoleProperties = db.EmployeeRoleProperties.ToList();
            return db.Employees.Find(User.Identity.GetUserId()).Roles.Any(
                x2 => EmployeeRoleProperties.Find(x3 => x3.ID == x2.ID).Level < Role.Level
            );
        }

        public static bool HigherThanUserRole(IPrincipal User, Employee Employee2)
        {
            DBContext db = new DBContext();
            List<Role> EmployeeRoleProperties = db.EmployeeRoleProperties.ToList();
            Employee Employee = db.Employees.Find(User.Identity.GetUserId());
            foreach (EmployeeRole Role in Employee.Roles)
            {
                foreach (EmployeeRole Role2 in Employee2.Roles)
                {
                    if(
                        EmployeeRoleProperties.Find(x => x.ID == Role.ID).Level
                        <
                        EmployeeRoleProperties.Find(x => x.ID == Role2.ID).Level
                    ){
                        return true;
                    }
                }
            }
            return false;
        }
    }
}