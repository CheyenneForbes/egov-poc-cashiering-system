﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using eGov_POC_Cashiering_System.Models;

namespace eGov_POC_Cashiering_System.Controllers
{
    public class EODReportsController : Controller
    {
        private readonly DBContext db = new DBContext();

        [Authorize]
        public ActionResult Index()
        {
            EODReportsViewModel obj;
            if (Session["EODReportsViewModel"] == null)
            {
                obj = new EODReportsViewModel();
                Session["EODReportsViewModel"] = obj;
            }
            else
            {
                obj = Session["EODReportsViewModel"] as EODReportsViewModel;
            }
            obj.EODReports = db.EODReports.ToList();
            return View(obj);
        }

        [Authorize]
        public ActionResult AddEODReport()
        {
            EODReport item = new EODReport
            {
                ClosedBy = User.Identity.GetUserId()
        };
            db.EODReports.Add(item);
            db.SaveChanges();
            return RedirectToAction("Index", new { editing = item.ID });
        }

        [Authorize]
        public ActionResult EditEODReport()
        {
            EODReport item = db.EODReports.Find(Request.Form["ID"]);
            return RedirectToAction("Index", new { editing = item.ID });
        }

        [Authorize]
        public ActionResult SaveEODReport()
        {
            EODReport item = db.EODReports.Find(Request.Form["ID"]);
            item.DateOpened = double.Parse(Request.Form["DateOpened"]);
            item.OpenCash = decimal.Parse(Request.Form["OpenCash"]);
            item.CloseDate = double.Parse(Request.Form["CloseDate"]);
            item.InHand = decimal.Parse(Request.Form["InHand"]);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        public ActionResult RemoveEODReport()
        {
            EODReport item = db.EODReports.Find(Request.Form["ID"]);
            db.EODReports.Remove(item);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}