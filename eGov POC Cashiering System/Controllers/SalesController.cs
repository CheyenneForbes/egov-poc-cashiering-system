﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using eGov_POC_Cashiering_System.Models;
using System.Data.Entity.Validation;

namespace eGov_POC_Cashiering_System.Controllers
{
    public class SalesController : Controller
    {
        private readonly DBContext db = new DBContext();

        [Authorize]
        public ActionResult Index()
        {
            SalesViewModel obj;
            if (Session["SalesViewModel"] == null)
            {
                obj = new SalesViewModel();
                Session["SalesViewModel"] = obj;
                obj.Transaction = new Transaction
                {
                    Type = "Sale"
                };
            }
            else
            {
                obj = Session["SalesViewModel"] as SalesViewModel;
            }
            return View(obj);
        }

        [Authorize]
        public ActionResult SetRegisterMode()
        {
            SalesViewModel obj = Session["SalesViewModel"] as SalesViewModel;
            obj.Transaction.Type = Request.Form["registerMode"];
            Session["SalesViewModel"] = obj;
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult AddItem()
        {
            SalesViewModel obj = Session["SalesViewModel"] as SalesViewModel;
            TransactionItem transactionItem = obj.Transaction.Items.Find(x => x.ItemId == Request.Form["itemToAdd"]);
            if (transactionItem == null)
            {
                Item item = db.Items.Find(Request.Form["itemToAdd"]);
                transactionItem = new TransactionItem
                {
                    ItemId = item.ID,
                    Name = item.Name,
                    Price = item.Price
                };
                obj.Transaction.Items.Add(transactionItem);
            }
            else
            {
                transactionItem.Quantity++;
            }
            Session["SalesViewModel"] = obj;
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult EditItem()
        {
            SalesViewModel obj = Session["SalesViewModel"] as SalesViewModel;
            TransactionItem item = obj.Transaction.Items.ToList().Find(x => x.ID == Request.Form["ID"]);
            Session["SalesViewModel"] = obj;
            return RedirectToAction("Index", new {editing = item.ID});
        }

        [Authorize]
        public ActionResult SaveItem()
        {
            SalesViewModel obj = Session["SalesViewModel"] as SalesViewModel;
            TransactionItem item = obj.Transaction.Items.ToList().Find(x => x.ID == Request.Form["ID"]);
            item.Quantity = int.Parse(Request.Form["quantity"]);
            Session["SalesViewModel"] = obj;
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult RemoveItem()
        {
            SalesViewModel obj = Session["SalesViewModel"] as SalesViewModel;
            TransactionItem item = obj.Transaction.Items.ToList().Find(x => x.ID == Request.Form["ID"]);
            Session["ItemsViewModel"] = obj;
            obj.Transaction.Items.Remove(item);
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult Complete()
        {
            SalesViewModel obj = Session["SalesViewModel"] as SalesViewModel;
            obj.Transaction.CustomerID = Request.Form["customerID"];
            obj.Transaction.PayeeID = Request.Form["payeeID"];
            obj.Transaction.PaymentAmount = decimal.Parse(Request.Form["paymentAmount"]);
            obj.Transaction.PaymentInstrument = Request.Form["paymentInstrument"];
            obj.Transaction.PaymentTimestamp = (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
            obj.Transaction.CashierID = User.Identity.GetUserId();
            obj.Transaction.CashierLocation = db.Employees.ToList().Find(x => x.ID == User.Identity.GetUserId()).Location;
            db.Transactions.Add(obj.Transaction);
            obj.CustomerName = Request.Form["customerName"];
            obj.PayeeName = Request.Form["payeeName"];
            db.SaveChanges();
            Session["SalesViewModel"] = null;
            return View("Completed", obj);
        }

        [Authorize]
        public ActionResult Cancel()
        {
            Session["SalesViewModel"] = null;
            return View("Index");
        }

        [Authorize]
        public ActionResult CancelReceipt()
        {
            Transaction receipt = db.Transactions.ToList().Find(x => x.ReceiptNumber == Request.Form["receiptNumber"]);
            if (receipt != null)
            {
                DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                DateTime Timestamp = Epoch.AddSeconds(receipt.PaymentTimestamp).ToLocalTime();
                if (DateTime.UtcNow.Date > Timestamp.Date)
                {
                    return RedirectToAction("Index", new { datePassed = true });
                }
                else if (db.EODReports.ToList().Find( x => Epoch.AddSeconds(x.CloseDate).ToLocalTime().Date == Timestamp.Date) != null)
                {
                    return RedirectToAction("Index", new { EODDone = true });
                }
                else
                {
                    receipt.CancelTimestamp = (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
                    db.SaveChanges();
                    return RedirectToAction("Index", new { receiptCancelled = true });
                }
            }
            else
            {
                return RedirectToAction("Index", new { receiptNotFound = true });
            }
        }
    }
}