﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using eGov_POC_Cashiering_System.Models;

namespace eGov_POC_Cashiering_System.Controllers
{
    public class ItemsController : Controller
    {
        private readonly DBContext db = new DBContext();

        [Authorize]
        public ActionResult Index()
        {
            ItemsViewModel obj;
            if (Session["ItemsViewModel"] == null)
            {
                obj = new ItemsViewModel();
                Session["ItemsViewModel"] = obj;
            }
            else
            {
                obj = Session["ItemsViewModel"] as ItemsViewModel;
            }
            obj.Items = db.Items.ToList();
            return View(obj);
        }

        [Authorize]
        public ActionResult AddItem()
        {
            Item item = new Item
            {
                Name = "Name here"
            };
            db.Items.Add(item);
            db.SaveChanges();
            return RedirectToAction("Index", new { editing = item.ID });
        }

        [Authorize]
        public ActionResult EditItem()
        {
            Item item = db.Items.Find(Request.Form["ID"]);
            return RedirectToAction("Index", new { editing = item.ID });
        }

        [Authorize]
        public ActionResult SaveItem()
        {
            Item item = db.Items.Find(Request.Form["ID"]);
            item.Name = Request.Form["Name"];
            item.Cost = decimal.Parse(Request.Form["Cost"]);
            item.Price = decimal.Parse(Request.Form["Price"]);
            item.Quantity = int.Parse(Request.Form["Quantity"]);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        public ActionResult RemoveItem()
        {
            Item item = db.Items.Find(Request.Form["ID"]);
            db.Items.Remove(item);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}