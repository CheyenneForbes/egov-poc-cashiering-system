﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using eGov_POC_Cashiering_System.Models;

namespace eGov_POC_Cashiering_System.Controllers
{
    public class InsightsController : Controller
    {
        private readonly DBContext db = new DBContext();

        [Authorize]
        public ActionResult Index()
        {
            InsightsViewModel obj;
            if (Session["InsightsViewModel"] == null)
            {
                obj = new InsightsViewModel();
                Session["InsightsViewModel"] = obj;
            }
            else
            {
                obj = Session["InsightsViewModel"] as InsightsViewModel;
            }
            obj.Items = db.Items.ToList();
            obj.Locations = db.Locations.ToList();
            obj.Employees = db.Employees.ToList();
            obj.EmployeeRoleProperties = db.EmployeeRoleProperties.ToList();
            obj.Transactions = db.Transactions.ToList();
            obj.LoginTrackers = db.LoginTrackers.ToList();
            return View(obj);
        }

        [Authorize]
        public ActionResult SetInsightType()
        {
            InsightsViewModel obj = Session["InsightsViewModel"] as InsightsViewModel;
            obj.InsightType = Request.Form["insightType"];
            Session["InsightsViewModel"] = obj;
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult SetLocation()
        {
            InsightsViewModel obj = Session["InsightsViewModel"] as InsightsViewModel;
            obj.Location = Request.Form["ID"];
            Session["InsightsViewModel"] = obj;
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult SetCashier()
        {
            InsightsViewModel obj = Session["InsightsViewModel"] as InsightsViewModel;
            obj.Cashier = Request.Form["ID"];
            Session["InsightsViewModel"] = obj;
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult SetStartDate()
        {
            InsightsViewModel obj = Session["InsightsViewModel"] as InsightsViewModel;
            obj.StartDate = (DateTime.Parse(Request.Form["StartDate"]) - new DateTime(1970, 1, 1)).TotalSeconds;
            Session["InsightsViewModel"] = obj;
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult SetEndDate()
        {
            InsightsViewModel obj = Session["InsightsViewModel"] as InsightsViewModel;
            obj.EndDate = (DateTime.Parse(Request.Form["EndDate"]) - new DateTime(1970, 1, 1)).TotalSeconds;
            Session["InsightsViewModel"] = obj;
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult SetSortBy()
        {
            InsightsViewModel obj = Session["InsightsViewModel"] as InsightsViewModel;
            obj.SortBy = Request.Form["sortBy"];
            Session["InsightsViewModel"] = obj;
            return RedirectToAction("Index");
        }
    }
}