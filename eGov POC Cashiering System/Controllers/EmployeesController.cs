﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using eGov_POC_Cashiering_System.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PostgreSQL.AspNet.Identity.EntityFramework;

namespace eGov_POC_Cashiering_System.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly DBContext db = new DBContext();

        [Authorize]
        public ActionResult Index()
        {
            EmployeesViewModel obj;
            if (Session["EmployeesViewModel"] == null)
            {
                obj = new EmployeesViewModel();
                Session["EmployeesViewModel"] = obj;
            }
            else
            {
                obj = Session["EmployeesViewModel"] as EmployeesViewModel;
            }
            obj.Employees = db.Employees.ToList();
            obj.EmployeeRoleProperties = db.EmployeeRoleProperties.ToList();
            obj.Locations = db.Locations.ToList();
            return View(obj);
        }

        [Authorize]
        public ActionResult AddEmployee()
        {
            EmployeesViewModel obj;
            if (Session["EmployeesViewModel"] == null)
            {
                obj = new EmployeesViewModel();
                Session["EmployeesViewModel"] = obj;
            }
            else
            {
                obj = Session["EmployeesViewModel"] as EmployeesViewModel;
            }
            obj.EmployeeRoleProperties = db.EmployeeRoleProperties.ToList();
            obj.Employees = db.Employees.ToList();
            obj.Locations = db.Locations.ToList();
            return View("AddEmployee", obj);
        }

        [Authorize]
        public ActionResult EditEmployee()
        {
            Employee Employee = db.Employees.Find(Request.Form["ID"]);
            return RedirectToAction("Index", new { editing = Employee.ID });
        }

        [Authorize]
        public ActionResult SaveEmployee()
        {
            Employee Employee = db.Employees.Find(Request.Form["ID"]);
            Employee.Name = Request.Form["Name"];
            Employee.UserName = Request.Form["UserName"];
            Employee.Email = Request.Form["Email"];
            Employee.Location = Request.Form["Location"];
            if (Request.Form["Roles"] != null)
            {
                List<string> roles = Request.Form["Roles"].Split(',').ToList();
                db.EmployeeRoles.ToList().ForEach(
                    x => {
                        if (x.UserID == Employee.ID)
                        {
                            db.EmployeeRoles.Remove(x);
                        }
                    }
                );
                roles.ForEach(
                    x => db.EmployeeRoles.Add(new EmployeeRole { UserID = Employee.ID, ID = x })
                );
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        public ActionResult RemoveEmployee()
        {
            Employee Employee = db.Employees.Find(Request.Form["ID"]);
            db.Employees.Remove(Employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}