﻿ CREATE TABLE "Privileges" (
  "Name" varchar(256) NOT NULL,
  "Group" varchar(256) NOT NULL,
  PRIMARY KEY ("Name")
);

 CREATE TABLE "RolePrivileges" ( 
  "RoleID" varchar(128) NOT NULL,
  "Name" varchar(256) NOT NULL,
  PRIMARY KEY ("RoleID", "Name")
);

CREATE TABLE "AspNetRoles" ( 
  "Id" varchar(128) NOT NULL DEFAULT lpad(nextval(format('%I', 'AspNetRolesID'))::text,9,'0'),
  "Name" varchar(256) NOT NULL,
  "Level" int,
  PRIMARY KEY ("Id")
);

CREATE TABLE "AspNetUsers" (
  "Id" character varying(128) NOT NULL,
  "UserName" character varying(256) NOT NULL,
  "Name" character varying(256) NOT NULL,
  "PasswordHash" character varying(256),
  "SecurityStamp" character varying(256),
  "Email" character varying(256) DEFAULT NULL::character varying,
  "EmailConfirmed" boolean NOT NULL DEFAULT false,
  "PhoneNumber" character varying(256),
  "PhoneNumberConfirmed" boolean NOT NULL DEFAULT false,
  "Location" character varying(256),
  "TwoFactorEnabled" boolean NOT NULL DEFAULT false,
  "LockoutEndDateUtc" timestamp,
  "LockoutEnabled" boolean NOT NULL DEFAULT false,
  "AccessFailedCount" int NOT NULL DEFAULT 0,

  PRIMARY KEY ("Id")
);

CREATE TABLE "AspNetUserClaims" ( 
  "Id" serial NOT NULL,
  "ClaimType" varchar(256) NULL,
  "ClaimValue" varchar(256) NULL,
  "UserId" varchar(128) NOT NULL,
  PRIMARY KEY ("Id")
);

CREATE TABLE "AspNetUserLogins" ( 
  "UserId" varchar(128) NOT NULL,
  "LoginProvider" varchar(128) NOT NULL,
  "ProviderKey" varchar(128) NOT NULL,
  PRIMARY KEY ("UserId", "LoginProvider", "ProviderKey")
);

CREATE TABLE "AspNetUserRoles" ( 
  "UserId" varchar(128) NOT NULL,
  "RoleId" varchar(128) NOT NULL,
  PRIMARY KEY ("UserId", "RoleId")
);

CREATE INDEX "IX_AspNetUserClaims_UserId"	ON "AspNetUserClaims"	("UserId");
CREATE INDEX "IX_AspNetUserLogins_UserId"	ON "AspNetUserLogins"	("UserId");
CREATE INDEX "IX_AspNetUserRoles_RoleId"	ON "AspNetUserRoles"	("RoleId");
CREATE INDEX "IX_AspNetUserRoles_UserId"	ON "AspNetUserRoles"	("UserId");

ALTER TABLE "RolePrivileges"
  ADD CONSTRAINT "FK_RolePrivileges_AspNetRoles_Role_ID" FOREIGN KEY ("RoleID") REFERENCES "AspNetRoles" ("Id")
  ON DELETE CASCADE;

ALTER TABLE "AspNetUserClaims"
  ADD CONSTRAINT "FK_AspNetUserClaims_AspNetUsers_User_Id" FOREIGN KEY ("UserId") REFERENCES "AspNetUsers" ("Id")
  ON DELETE CASCADE;

ALTER TABLE "AspNetUserLogins"
  ADD CONSTRAINT "FK_AspNetUserLogins_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES "AspNetUsers" ("Id")
  ON DELETE CASCADE;

ALTER TABLE "AspNetUserRoles"
  ADD CONSTRAINT "FK_AspNetUserRoles_AspNetRoles_RoleId" FOREIGN KEY ("RoleId") REFERENCES "AspNetRoles" ("Id")
  ON DELETE CASCADE;

ALTER TABLE "AspNetUserRoles"
  ADD CONSTRAINT "FK_AspNetUserRoles_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES "AspNetUsers" ("Id")
  ON DELETE CASCADE;

CREATE TABLE "LoginTrackers" ( 
    "Location" varchar(256) NOT NULL,
    "UserID" varchar(256) NOT NULL,
    "LoginDateTime" double precision,
    "LogoutDateTime" double precision,
    PRIMARY KEY ("UserID", "LoginDateTime")
);

CREATE TABLE "Locations" ( 
  "ID" varchar NOT NULL DEFAULT lpad(nextval(format('%I', 'LocationsID'))::text,9,'0'),
  "Name" varchar(256) NOT NULL
);

CREATE TABLE "Items"("ID" varchar NOT NULL DEFAULT lpad(nextval(format('%I', 'ItemsID'))::text,9,'0'), "Name" varchar, "Cost" decimal, "Price" decimal, "Quantity" int, PRIMARY KEY ("ID"));

CREATE TABLE "EODReports"(
    "ID" varchar NOT NULL DEFAULT lpad(nextval(format('%I', 'EODReportsID'))::text,9,'0'),
    "DateOpened" double precision,
    "OpenCash" decimal,
    "CloseDate" double precision,
    "ClosedBy" varchar,
    "InHand" decimal
);

CREATE TABLE "Transactions"("ReceiptNumber" varchar NOT NULL DEFAULT lpad(nextval(format('%I', 'TransactionsID'))::text,9,'0'), "Type" varchar, "CustomerID" varchar, "PayeeID" varchar, "PaymentAmount" decimal, "PaymentInstrument" varchar, "PaymentTimestamp" double precision, "CancelTimestamp" double precision, "CashierID" varchar, "CashierLocation" varchar, PRIMARY KEY ("ReceiptNumber"));

CREATE TABLE "TransactionItems"("ID" varchar NOT NULL DEFAULT lpad(nextval(format('%I', 'TransactionItemsID'))::text,9,'0'), "ReceiptNumber" varchar, "ItemId" varchar, "Name" varchar, "Quantity" int, "Price" decimal, PRIMARY KEY ("ID"));

ALTER TABLE "TransactionItems"
ADD CONSTRAINT receipt_number_fk FOREIGN KEY ("ReceiptNumber") REFERENCES "Transactions"("ReceiptNumber");

ALTER TABLE "TransactionItems"
ADD CONSTRAINT item_id_fk FOREIGN KEY ("ItemId") REFERENCES "Items"("ID");

ALTER TABLE "Locations"
ADD CONSTRAINT Location_pk PRIMARY KEY ("ID");

ALTER TABLE "AspNetUsers"
ADD CONSTRAINT employee_location_fk FOREIGN KEY ("Location") REFERENCES "Locations"("ID");

CREATE SEQUENCE "AspNetRolesID" START WITH 1;

CREATE SEQUENCE "EODReportsID" START WITH 1;

CREATE SEQUENCE "ItemsID" START WITH 1;

CREATE SEQUENCE "ReceiptNumber" START WITH 1;

CREATE SEQUENCE "TransactionItemsID" START WITH 1;

CREATE SEQUENCE "LocationsID" START WITH 1;

CREATE SEQUENCE "ReportID" START WITH 1;

INSERT INTO
    "Locations"("Name")
    VALUES(
        'Main'
    );

INSERT INTO
    "Privileges"("Name", "Group")
    VALUES(
        'AddEmployee',
        'Employees'
    ),(
        'EditEmployee',
        'Employees'
    ),(
        'RemoveEmployee',
        'Employees'
    ),(
        'AddEODReport',
        'EODReports'
    ),(
        'EditEODReports',
        'EODReports'
    ),(
        'RemoveEODReports',
        'EODReports'
    ),(
        'AddItems',
        'Items'
    ),(
        'EditItems',
        'Items'
    ),(
        'RemoveItems',
        'Items'
    ),(
        'Sale',
        'Sales'
    ),(
        'Return',
        'Sales'
    ),(
        'CancelReceipt',
        'Sales'
    ),(
        'ManageRoles',
        'Settings'
    ),(
        'ManageLocations',
        'Settings'
    ),(
        'View',
        'Insights'
    );

INSERT INTO
    "AspNetRoles"("Name", "Level")
    VALUES(
        'Administrator',
        1
    );

INSERT INTO
    "AspNetUsers"("Id", "Name", "UserName", "PasswordHash", "SecurityStamp", "Email", "Location")
    VALUES(
        'fe073317-681e-4746-b7de-5169421322b7',
        'Main Admin',
        'Admin',
        'AIwX/xtJ02DqsvH8QDdCnvEO2QCbXiQuhAhTNv6XPDsggovzyD6LMAIC2ihozYsJbA==',
        'f62b2b0d-d39f-4f25-9226-9d40c996f1b2',
        'cashadmin@egovja.com',
        '000000001'
    );

INSERT INTO
    "AspNetUserRoles"("UserId", "RoleId")
    VALUES(
        'fe073317-681e-4746-b7de-5169421322b7',
        '000000001'
    );