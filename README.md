## eGov POC  Cashiering System

### Stack
- ASP.Net MVC v5.2.7 (C#)
- PostgreSQL v9
- Credentials are validated via User Database using **PostgreSQL.AspNet.Identity.EntityFramework**

### Key notes
- All new accounts are created with the password **Password123!** (including the default Administrator account with user name **Admin**).
- As a security precaution, it is recommended that the default password is changed after the first login.
- Receipts can only be cancelled if the date is in the past or the EOD is not yet performed for that date.

### Features
- Compatible with all major browsers.
- Role based users **[Employees]**.
- Privilege based roles **[Settings -> manage  roles]**.
- Multi-location **[Settings -> manage  locations]**.
- Stock management **[Items]**.
- Sale execution **[Sales -> Mode:Sale]**.
- Return of sold items **[Sales -> Mode:Return]**.
- Receipt cancellation **[Sales -> Mode:Cancel Receipt]**.
- Multiple payment methods.
- Generation and printing of receipts.
- EOD Reconciliation **[EOD]**.
- Cashier Transaction Report **[Insights -> Type:Cashier Transaction Report]**.
- Cashier Summary Report **[Insights -> Type:Cashier Summary Report]**.
- Cashiers Logged On **[Insights -> Type:Cashiers Logged On]**
- Printing of reports.

### Privileges
- **Employees->AddEmployee**
- **Employees->EditEmployee**
- **Employees->RemoveEmployee**
- **EODReports->AddEODReport**
- **EODReports->EditEODReports**
- **EODReports->RemoveEODReports**
- **Items->AddItems**
- **Items->EditItems**
- **Items->RemoveItems**
- **Sales->Sale**
- **Sales->Return**
- **Sales->CancelReceipt**
- **Settings->ManageRoles**
- **Settings->ManageLocations**
- **Insights->View**